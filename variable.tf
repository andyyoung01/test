variable "location" {
  description = "location"
  type        = string
  default = "eastus"
}

variable "prefix" {
  description = "prefix"
  type        = string
  default     = "example"
}